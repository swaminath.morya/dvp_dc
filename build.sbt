version in ThisBuild := "0.1.0-SNAPSHOT"

scalaVersion in ThisBuild := "2.11.12"

lazy val root = (project in file("."))
  .settings(
    name := "DVP_DC"
  )

libraryDependencies += "org.apache.livy" % "livy-client-http" % "0.7.1-incubating"
val sparkVersion = "2.4.5"
val hadoopVersion = "3.1.1"
val hiveVersion = "3.1.3"
libraryDependencies ++= Seq( "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "com.typesafe" % "config" % "1.4.2",
  "javax.mail" % "mail" % "1.4.7",
  "org.slf4j" % "slf4j-api" % "1.7.6" % "provided",
  "org.apache.httpcomponents" % "httpclient" % "4.3.6" % "provided",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.4.4" % "provided",
  "com.databricks" % "spark-csv_2.11" % "1.0.3",
  "org.joda" % "joda-convert" % "1.8.1")

